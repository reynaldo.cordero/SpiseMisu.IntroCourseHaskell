module Book.Format
  ( Audio
    ( AAC, MP3, M4B, WAV
    )
  , Electronic
    ( EPUB, MOBI, PDF
    )
  , Physical
    ( Hardcover
    , Paperback
    )
  ) where

--------------------------------------------------------------------------------

data Audio      = AAC  | MP3  | M4B | WAV
data Electronic = EPUB | MOBI | PDF
data Physical   = Hardcover   | Paperback
