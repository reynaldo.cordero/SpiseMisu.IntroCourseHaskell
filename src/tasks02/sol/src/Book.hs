module Book
  ( Author
    (..)
  , Language
    (..)
  , ISBN10
    (..)
  , ISBN13
    (..)
  , Common
    (..)
  , Book
    (..)
  , Audio
    (..)
  , Electronic
    (..)
  , Physical
    (..)
  ) where

--------------------------------------------------------------------------------

import qualified Book.Format as Format

--------------------------------------------------------------------------------

type Name    = String
type Surname = String

data Author   = Author Name Surname
data Language = English | Danish | Spanish
data ISBN10   = ISBN10 Integer
data ISBN13   = ISBN13 Integer

data Common = Common
  { title     :: String
  , authors   :: [ Author ]
  , publisher :: String
  , language  :: Language
  , isbn10    :: ISBN10
  , isbn13    :: ISBN13
  }
data Book
  = Audio      Audio
  | Electronic Electronic
  | Physical   Physical
data Audio = Audio
  { common :: Common
  , format :: Format.Audio
  }
data Electronic = Electronic
  { common :: Common
  , pages  :: Integer
  , format :: Format.Electronic
  }
data Physical = Physical
  { common :: Common
  , pages  :: Integer
  , format :: Format.Physical
  }
