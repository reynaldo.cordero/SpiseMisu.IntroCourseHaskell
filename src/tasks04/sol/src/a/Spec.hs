#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --package hspec
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

module Spec (main) where

--------------------------------------------------------------------------------

import           Test.Hspec
    ( Spec
    , describe
    , hspec
    , it
    , shouldBe
    )

--------------------------------------------------------------------------------

cartProd
  :: [a]
  -> [b]
  -> [(a, b) ]

unittests
  :: [ Spec ]

testCase
  :: IO ()

main
  :: IO ()

--------------------------------------------------------------------------------

cartProd xs ys =
  (,) <$> xs <*> ys

unittests =
  [ it "cartProd [1,2] [3,4] == [(1,3),(1,4),(2,3),(2,4)]" $
    cartProd xs ys `shouldBe` [(1,3),(1,4),(2,3),(2,4)]
  ]
  where
    xs = [1,2] :: [ Integer ]
    ys = [3,4] :: [ Integer ]

testCase =
  hspec $
  do
    describe "Unit Testing" $
      do
        mapM_ id unittests

main =
  do
    putStrLn $ "Test case based on Hspec"
    testCase
