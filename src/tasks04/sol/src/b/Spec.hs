#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --package hspec
   --package QuickCheck
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

module Spec (main) where

--------------------------------------------------------------------------------

import           Test.Hspec
    ( Spec
    , describe
    , hspec
    , it
    )
import           Test.QuickCheck
    ( property
    )

--------------------------------------------------------------------------------

proptests
  :: [ Spec ]

propCase
  :: IO ()

main
  :: IO ()

--------------------------------------------------------------------------------

proptests =
  [ it "reverse (reverse xs) == xs" $ property twice
  ]
  where
    twice :: [ Char ] -> Bool
    twice = \xs -> reverse (reverse xs) == xs

propCase =
  hspec $
  do
    describe "Propety-base Testing" $
      do
        mapM_ id proptests

main =
  do
    putStrLn $ "Property case based on Hspec + QuickCheck"
    propCase
