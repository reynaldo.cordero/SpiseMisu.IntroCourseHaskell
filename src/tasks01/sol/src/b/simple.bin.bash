#!/bin/bash

bin="$1"

stack \
    --resolver lts-12.0 \
    --install-ghc \
    new "$bin" simple
