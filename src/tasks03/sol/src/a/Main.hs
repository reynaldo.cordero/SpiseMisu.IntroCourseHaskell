#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

cartProd
  :: [a]
  -> [b]
  -> [(a, b) ]

main
  :: IO ()

--------------------------------------------------------------------------------

cartProd xs ys =
  (,) <$> xs <*> ys

main =
  do
    putStrLn "cartProd [1,2] [3,4] == [(1,3),(1,4),(2,3),(2,4)]"
    putStrLn $ show $ cartProd xs ys == [(1,3),(1,4),(2,3),(2,4)]
    where
      xs = [1,2] :: [ Integer ]
      ys = [3,4] :: [ Integer ]
