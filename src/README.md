Tasks
=====

## Basics

### Tasks 00

* a) Implement a tool that reverses input:

  - **Example**: `echo -n “Some Text” | ./reverse`
  
  - **Hint**: [Prelude.interact][interact]

* b) Implement a data type containing a person’s names (first and last)

  - **Example**: `Name "John" "Doe"`
  
  - **Hint**: ADT (records)

**Note**: [Hoogle][hoogle] is really good to find already implemented logic:
Search for the following signature: [[a] -> [a]][signature]

[interact]:  http://hackage.haskell.org/package/base-4.11.1.0/docs/Prelude.html#v:interact
[hoogle]:    https://www.haskell.org/hoogle/
[signature]: https://www.haskell.org/hoogle/?hoogle=%5Ba%5D+->+%5Ba%5D


## Stack

### Tasks 01

* a) Convert Task.00.a from a binary to a script

  - **Example**: `echo -n “Some Text” | ./Main.hs`

* b) By using stack templates, create a simple binary project

* c) By using stack templates, create a simple library project

**Note**: When calling stack for a), b) and c), ensure that you use the
[Long-Term Support version 12.0][stack] and if it’s not present at your system,
that it should be downloaded and sanboxed:

	user@personal:$ stack --resolver lts-12.0 --install-ghc ...

[stack]: https://www.stackage.org/lts-12.0


## Type Driven Development (TDD)

### Tasks 02

Implement the domain of a Book, that could be used for a Bookstore or a Library:

* Types: Audio, electronic and physical

* Formats:
	- AAC, MP3, M4B and WAV
	- EPUB, MOBI and PDF
	- Hardcover and Paperback

* Info:
	- Mandatory: title, authors, publisher, language, ISBN10 and ISBN13
	- Optional: pages


## High-order functions (HOF) + TypeClasses (TC)

### Tasks 03

![JSON Object][json]

* a) Implement the [Cartesian product][cartprod]

  - **Example**: `cartProd [1,2] [3,4] == [(1,3),(1,4),(2,3),(2,4)]`
  
  - **Hints**: [(,)][tuple], [(<$>)][fmap] and [(<*>)][applicative]
  

* b) Implement the JSON value as a (recursive) data structure Provide an
instances for the Show Type-class to print out a valid JSON string instance Show
Value where ...

[cartprod]:    https://en.wikipedia.org/wiki/Cartesian_product
[tuple]:       http://hackage.haskell.org/package/ghc-prim-0.5.2.0/docs/GHC-Tuple.html#t:-40--44--41-
[fmap]:        http://hackage.haskell.org/package/base-4.11.1.0/docs/Prelude.html#v:-60--36--62-
[applicative]: http://hackage.haskell.org/package/base-4.11.1.0/docs/Prelude.html#v:-60--42--62-

[json]:        img/task03_json.png


## Testing

### Tasks 04

* a) Write a Unit-test to check if the `cartProd` function from `Task.03.a`
works as expected:

  - **Example**: `cartProd [1,2] [3,4] == [(1,3),(1,4),(2,3),(2,4)]'`

* b) Write a Property-based test to check if the reverse function from Task.00.a
works as expected:

  - **Example**: `(reverse $ reverse “Some Text”) == “Some Text”`


## Profiling

### Tasks 05

* a) Implement a naive byte counter and profile it:

  - **Example**: `cat naive_count | ./naive_count +RTS -h`
  
  - **Hint**: [Data.ByteString.Lazy.interact][bytestring]

* b) Add an accumulator to the naive byte counter and profile it:

  - **Example**: `cat acc_count | ./acc_count +RTS -h`

**Note**: Generate a graphical visualization for both with:

	hp2ps -c naive_count.hp and hp2ps -c acc_count.hp

[bytestring]: http://hackage.haskell.org/package/bytestring-0.10.8.2/docs/Data-ByteString-Lazy.html#v:interact


## Safe

### Tasks 06

* a) Import the safe package [Data.Time][time] to your script

* b) Import the package [Data.Aeson][aeson] to your script

**Note**: When executing the scripts, ensure that both have the safe language
pragma or compiler flags. Also, packages are imported to scripts by simple
adding:

```
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --package time
   –
-}
```

[time]:  http://hackage.haskell.org/package/time-1.9.2/docs/Data-Time.html
[aeson]: http://hackage.haskell.org/package/aeson-1.4.0.0/docs/Data-Aeson.html

## Side-effects

### Tasks 07

* a) Limit your script so it only can print to the console

* b) Add support so it can also read input characters

**Note**: We are going to re-use the same approach that we used for `Tasks.03.b`
when creating instances for Type-classesTasks
