module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy       as LBS
import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

count :: LBS.ByteString -> Integer
count =
  aux 0
  where
  aux acc bs
    | LBS.null bs = acc
    | otherwise   = aux (1 + acc) (LBS.tail bs)

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  LBS.interact $ L8.pack . show . count
