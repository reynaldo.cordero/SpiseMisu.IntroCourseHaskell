#!/bin/bash

clear

./clear.bash

# naive_count (+ profiling)
ghc -prof -fprof-auto -rtsopts -Wall -Werror -O2 --make Main.hs -o naive_count

# run and generate memory profile
#cat ./naive_count | ./naive_count +RTS -h && echo
cat ~/downloads/debian-9.5.0-amd64-netinst.iso | ./naive_count +RTS -h && echo

# create a graph of memory profile
hp2ps -c naive_count.hp

# clean
find . -name '*.hi' -delete
find . -name '*.o'  -delete
