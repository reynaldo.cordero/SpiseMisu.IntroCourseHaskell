#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --package time
   --package hspec
   --package QuickCheck
   --ghc-options -threaded
   --
-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           System.IO.Unsafe
    ( unsafePerformIO
    )

import           Data.Time
    ( UTCTime
    , defaultTimeLocale
    , parseTimeOrError
    )

import           Test.Hspec
    ( describe
    , hspec
    , it
    , shouldBe
    )
import           Test.QuickCheck
    ( property
    )

-------------------------------------------------------------------------------

basicsGHCI :: [ Char ]
basicsGHCI =
  ['a'] ++ ['b'] ++ ['c']
  -- :t (++)
  -- :i Char

-------------------------------------------------------------------------------

basicsSyntax :: IO ()
basicsSyntax =
  mapM_ (putStr . show) $ take 5 pair
  -- (1,2)(1,2)(1,2)(1,2)(1,2)
  where
    ones :: Num a => [a]
    ones = 1 : ones       -- Infinite sequence of 1’s

    twos :: Num a => [a]
    twos = map (+ 1) ones -- Infinite sequence of 2’s

    pair :: (Num a, Num b) => [(a, b)]
    pair = zip ones twos  -- Infinite sequence of (1,2)’s

--------------------------------------------------------------------------------

basicsReadability :: IO ()
basicsReadability =
  do
    putStrLn =<< foo
    putStrLn =<< bar
  where
  foo :: IO String
  foo =
    -- (=<<) Lifts impure values (IO) into purity
    pure . show . length . words =<< getLine
  bar :: IO String
  bar = do
    input <- getLine -- (<-) Lifts impure values (IO) into purity
    let count = length $ words $ input
    return $ show $ count

--------------------------------------------------------------------------------

data FooBar  = FooBar Integer Char
data FooBar' = FooBar' { foo :: Integer, bar :: Char }

data Person = Child | Adult
data Temperature = C Double | F Integer

data Boots    = Boots
data Ball     = Ball
data Computer = Computer
data Software = Software
data Football = Football Boots Ball
data Dota2    = Dota2 Computer Software
data Sport    = Classic Football | Esport Dota2

basicsADT :: IO ()
basicsADT =
  do
    putStrLn $ show $ 42 == (foo $ FooBar' 42 'x')
    putStrLn $ show $ isFoo42    $ FooBar' 42 'x'
    putStrLn $ show $ assertAge 17 Adult
    where
      isFoo42 :: FooBar' -> Bool
      isFoo42 fb
        | 42 == foo fb = True
        | otherwise    = False

      assertAge :: Integer -> Person -> Bool
      assertAge age p =
        case p of
          Child -> age <  18
          Adult -> age >= 18

--------------------------------------------------------------------------------

data Booking
  = Basic    Plane
  | Combo    Combo
  | FullPack Plane Hotel Car
  deriving Show
data Combo
  = WithHotel Plane Hotel
  | WithCar   Plane Car
  deriving Show
data Plane = Plane
  { departure   :: UTCTime
  , arrival     :: UTCTime
  , destination :: City
  }
  deriving Show
newtype Hotel = Hotel { hotel :: String }
  deriving Show
newtype Car   = Car   { car   :: String }
  deriving Show
newtype City  = City  { city  :: String }
  deriving Show

madrid :: Booking
madrid =
  FullPack
  (
    Plane
    (toUTC "2018-08-08T15:40:00Z")
    (toUTC "2018-08-15T12:15:00Z")
    (City  "Madrid")
  )
  (Hotel "Eurostars Madrid Tower")
  (Car   "Tesla Model S")

toUTC :: String -> UTCTime
toUTC iso8601 =
  parseTimeOrError True defaultTimeLocale "%FT%T%0QZ" iso8601

--------------------------------------------------------------------------------

withMaybe =
  do
    putStrLn $ show $        (+ 1) <$> Just 42
    putStrLn $ show $ Just   (+ 1) <*> Just 42
    putStrLn $ show $ pure . (+ 1) =<< Just 42

withList =
  do
    putStrLn $ show $        (+ 1)  <$> [42, 43]
    putStrLn $ show $ [(+ 2),(+ 1)] <*> [42, 43]
    putStrLn $ show $ pure . (+ 1)  =<< [42, 43]

class (Integral a, Eq a) => Odd a where
  isOdd :: a -> Bool
  isOdd x = x `mod` 2 == 1

-- Equivalent to Maybe type
data Result a = OK a | Error deriving Show

instance Functor Result where
  fmap _ Error  = Error
  fmap f (OK a) = OK $ f a

instance Applicative Result where
  pure              a  = OK      a
  (<*>) (OK fn) (OK a) = OK $ fn a
  (<*>) _______ ______ = Error

instance Monad Result where
  return = pure
  (>>=) (OK a) f = f a
  (>>=) ______ _ = Error

--------------------------------------------------------------------------------

unittests =
  [ it "1 times 0 = 0"  $ (1 * 0) `shouldBe`  0
  , it "4 times 5 = 20" $ (4 * 5) `shouldBe` 20
  ]

testCase =
  hspec $
  do
    describe "Unit Testing" $
      do
        mapM_ id unittests

proptests =
  [ it " x * y      equals      y * x " $ property commutative
  , it "(x * y) * z equals x * (y * z)" $ property associative
  ]
  where
    commutative :: Int -> Int -> Bool
    commutative = \x y -> x * y == y * x
    associative :: Int -> Int -> Int -> Bool
    associative = \x y z -> (x * y) * z == x * (y * z)

propCase =
  hspec $
  do
    describe "Propety-base Testing" $
      do
        mapM_ id proptests

--------------------------------------------------------------------------------

count [    ] = 0
count (x:xs) = 1 + count xs

count' [    ] acc = acc
count' (x:xs) acc = count' xs (acc + 1)

-- {-# LANGUAGE Strict #-}
reduce :: (t -> a -> t) -> t -> [a] -> t
reduce   _ acc [    ] = acc
reduce   f acc (x:xs) = reduce f (f acc x) xs

reduce' :: (t -> a -> t) -> t -> [a] -> t
reduce'  _ acc [    ] = acc
reduce'  f acc (x:xs) = acc' `seq` reduce' f acc' xs
  where acc' = f acc x

reduce'' :: (t -> a -> t) -> t -> [a] -> t
reduce'' _ acc [    ] = acc
reduce'' f acc (x:xs) = reduce'' f acc' xs
  where !acc' = f acc x

--------------------------------------------------------------------------------

reftrans :: Char
reftrans = unsafePerformIO $ pure =<< getChar

--------------------------------------------------------------------------------

main :: IO ()
main =
  do
    putStrLn $ basicsGHCI
    basicsSyntax
    putStrLn $ ""
    putStrLn $ "Type (twice) a few words:"
    basicsReadability
    basicsADT
    putStrLn $ show $ madrid
    putStrLn $ "Test case based on Hspec"
    testCase
    putStrLn $ "Property case based on Hspec + QuickCheck"
    propCase
